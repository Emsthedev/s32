let http = require('http')

let port = 4000

const server = http.createServer(function(request, response) {
	if(request.url == '/' && request.method == 'GET') { 
	response.writeHead(200, {'Content-Type': 'text/plain'})
	
	response.end('Welcome to booking system')
	} else if(request.url == '/profile' && request.method == 'GET') { 
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Welcome to your profile')
	}else if(request.url == '/courses' && request.method == 'GET') { 
        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end('Here is our courses available')
        }else if(request.url == '/addCourse' && request.method == 'POST') { 
            response.writeHead(200, {'Content-Type': 'text/plain'})
            response.end('Add course to our resources')
            }else if(request.url == '/updateCourse' && request.method == 'PUT') { 
                response.writeHead(200, {'Content-Type': 'text/plain'})
                response.end('Update course to our resources')
                }else if(request.url == '/archiveCourse' && request.method == 'DELETE') { 
                    response.writeHead(200, {'Content-Type': 'text/plain'})
                    response.end('Archive course to our resources')
                    }
	
}).listen(port);

console.log(`Server is now accessible at localhost: ${port}`);

